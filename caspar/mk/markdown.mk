# This file is maintained at http://git.mdcc.cx/caspar
#
# markdown.mk - typeset documentation from CommonMark .md files .
#
# We call cmark by John MacFarlane e.a., see
# https://code.commonmark.org . The CommonMark format is a version
# of Markdown syntax with a spec.
#
# See https://bugs.debian.org/466330 for some notes on traditonal markdown
# vs CommonMark in Debian and elsewhere.
#
# See caspar-typesetting(7) for usage info.
#
# this Makefile snippet needs GNU Make

# Copyright (C) 2022, 2024 Joost van Baal-Ilić <joostvb-caspar-c-12@mdcc.cx>
#
# This file is part of caspar.  Caspar is free software; you can redistribute
# it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the License,
# or (at your option) any later version.  You should have received a copy of
# the GNU General Public License along with this file (see COPYING).

MDS := $(shell for f in *.md; do test -f $$f && echo -n $$f " "; done)
BASES       := $(basename $(MDS))
HTMLS       := $(patsubst %,%.html,$(BASES))

typeset: $(HTMLS)

%.html: %.md
	cmark --to html $< >$@

clean:
	-rm -f $(HTMLS)
